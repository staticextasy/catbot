#This file will serve as functions for use in the main CatBot program.

import re

#Create a Word Filter function.
def checkFilter(word):
    with open("tagfilter.txt", 'r') as file:
        if re.search('^{0}$'.format(re.escape(word)), file.read(), flags=re.M):
            return True
        else:
            return False

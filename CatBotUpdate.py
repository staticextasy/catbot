# CatBot https://gitlab.com/staticextasy/catbot
# CatBot by StaticExtasy

import requests

url = 'https://gitlab.com/staticextasy/catbot/raw/master/CatBot.py'

print ("downloading catbot.py")
r = requests.get(url)
with open("CatBot.py", "wb") as code:
    code.write(r.content)

print ("CatBot.py downloaded...")
print ("Launch Catbot with: python CatBot.py")

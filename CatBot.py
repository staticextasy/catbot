#CatBot for @Comrade on Discord
#CatBot by StaticExtasy u/staticextasy
#CatBot Version 0.11.5

#REMEMBER TO INSTALL giphypop

import discord # the discord.py to which this bot is built on.
import giphypop #Required to search from Giphy by some commands
import random
import catFunctions
from discord.ext import commands

description = '''The Mighty CatBot.'''

bot = commands.Bot(command_prefix='*', description=description) # Do not modify the description, only the prefix.

TOKEN = "" #Enter your Bot API Token here. Get your Bot account token from https://discordapp.com/developers/applications/

g = giphypop.Giphy() # declaring giphypop as g

@bot.event
async def on_message(message) :
    # we do not want the bot to reply to itself
    if message.author == bot.user:
        return

    #Displays if the word meow is said in chat by anyone.
    if "meow" in message.content:
        msg = '(=^･^=) *purrs on {0.author.mention}*'.format(message)
        await bot.send_message(message.channel, msg)
    await bot.process_commands(message)
    
@bot.event
async def on_ready():
    await bot.change_presence(game=discord.Game(name="with human souls")) # Change "with human souls" to anything you would like the bot to display as it's playing title.
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    
#Converts Celcius to Freedomheit and vice versa.
@bot.command()
async def convert(temp):
    '''Converts Celcius to Freedomheit and vice versa.'''
    ttype = temp[-1:]
    tnum = temp[:-1]
        
    if ttype.lower() == 'f' :
        convert_value = (int(tnum) - 32) * 5/9
        tempature = str(temp) + " to Celcius is " + str(convert_value) + "c"

    elif ttype.lower() == 'c' :
        convert_value = (int(tnum) * 1.8) + 32
        tempature = str(temp) + " to Fahrenheit is " + str(convert_value) + "f"
    else:
        tempature = "That's not a scale of tempature I can read. Use Freedomheit or Celcius"
    await bot.say(tempature)

#Search and Display a random Cat Gif from Giphy
@bot.command()
async def catgif():
    """Displays a random Cat Gif from Giphy"""
    catgif = g.random_gif('cat', 1)
    await bot.say(catgif)
    
#Search a random gif with a Tag and Display from Giphy
@bot.command()
async def gif(tag):
    """Display a random gif from a search on giphy. *gif [tag], Example: *gif dogs"""
    gif = g.random_gif(tag)
    if catFunctions.checkFilter(tag): #Checking if tag is in the filter list
        gif = "Meow! I can't say that!"
    await bot.say(gif)
    
@bot.command()
async def add(left : int, right : int):
    """Adds two numbers together."""
    await bot.say(left + right)
    
@bot.command()
async def subtract(left : int, right : int):
    """Subtracts two numbers together."""
    await bot.say(left - right)

@bot.command()
async def roll(dice : str):
    """Rolls a dice in NdN format."""
    catTalk = ('boops the die with its nose', 'smacks the die around', 'whacks with its tail')
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        await bot.say('Format has to be in NdN!')
        return
    result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
    await bot.say('*{0}*'.format(random.choice(catTalk)))
    await bot.say(result)

@bot.command(description='For when you wanna settle the score some other way')
async def choose(*choices : str):
    """Chooses between multiple choices."""
    await bot.say(random.choice(choices))

@bot.command()
async def flip():
    """A command that will flip a coin, and choose "Heads" or "Tails"."""
    choices = ('Heads!', 'Tails!')   
    await bot.say(random.choice(choices))

bot.run(TOKEN)

# CatBot

Discord Chatbot using Discord.py - More Information here https://github.com/Rapptz/discord.py
CatBot is released under the MIT License.


### Requirements

* Discord.py - Get it at https://github.com/Rapptz/discord.py

Follow the instructions there to get started with Discord.py

* Python Requests
* Giphypop for Giphy Images.


use `pip install requests giphypop`


### What CatBot can do:

At the current time all commands are prefixed with an Asterik(*)

##### List of Commands
* meow - Good kitty, loves it's master
* convert - Will convert Celcius to Freedomheit or vice versa
* catgif - Searches Giphy for a random Cat Gif.
* gif - Search for a random gif tag. gif is Filtered by the tagfilter.txt Example: `*gif dogs`
* roll - Roll a die in the form of NdN. Example: `*roll 1d10` This rolls a 10 sided dice 1 time.
* choose - Chooses between two given options. Example: `*choose cats dogs`
* flip - Flip a coin for Heads or Tails.
* add - Add two numbers together
* subtract - Opposite of above...
* help - display all available commands. 


### FAQ

#### How do I Filter out a word from gif search?
Included is a text file called tagfilter.txt, Add your words on new lines in that file.

#### I get a warning about giphypop 
`UserWarning: You are using the giphy public api key. This should be used for testing only and may be deactivated in the future. See https://github.com/Giphy/GiphyAPI. warnings.warn('You are using the giphy public api key. This '`

You can ignore this warning for now. giphypop uses the public api key to grab images from their site. It can change eventually so beware the event that this breaks.